'''
Description:
SocialPerson class object to manage friend/colleague list
'''
import sqlite3
from email_db import *

class SocialPerson:
    def __init__(self,fname=None):
        self.friends = {}
        if not fname:
            self.me = Person("","","","")
            self.status = input('Enter my status: ')
        else:
            file = open(fname)
            self.me = Person.read_person(file)
            self.status = file.readline()[:-1]
            friend = Person.read_person(file)
            while friend:
                self.friends[friend.email] = friend
                friend = Person.read_person(file)
                
    def friends_str(self):
        if self.friends == {}:
            return ''
        else:
            i = 1
            friends = ''
            for key in sorted(self.friends.keys()):
               friends += str(i)+')  '+ str(self.friends[key])+'\n'
               i+=1
            return friends
            
    def __repr__(self):
        first = '---------- me ----------\n'
        status = 'My status is: '+ self.status + '\n'
        friend = '\n------- friends --------\n'+ self.friends_str()
        return first+str(self.me)+'\n'+status+friend
    
    def add_friend(self):
        p = Person()
        self.friends[p.email] = p
        
    def get_key(self):
        print('------- friends --------\n'+self.friends_str()+'------------------------')
        friend_num = int(input('Enter friend number or 0 to cancel: '))
        if friend_num<0 or friend_num>len(self.friends):
            print('Not a friend number: '+str(friend_num))
            return ''
        elif friend_num==0:
            return ''
        else:
            return sorted(self.friends)[friend_num-1]
            
    def unfriend(self):
        email = self.get_key()
        if email != '':
            self.friends.pop(email)
    
    def write_sp(self, fname):
        file = open(fname, 'w')
        self.me.write_person(file)
        file.write(self.status+'\n')
        for key in self.friends:
            self.friends[key].write_person(file)
        file.close()
        
    def get_sp():
        menu = '---------- SocialPerson Options ----------\n'
        menu += '1) Create a new SocialPerson\n' 
        menu += '2) Load a SocialPerson from file\n'
        menu += '3) Cancel'
        print(menu)
        option = input('Enter option number: ')
        if option=='2' or option=='2)':
            file_name = input('Enter filename: ')
            return SocialPerson(file_name)
        if option=='1' or option=='1)':
            return SocialPerson()
    
    def get_option():
        opt = '---------- SocialPerson Options -----------\n'
        opt += '1) Add a friend\n'
        opt += '2) Unfriend someone\n'
        opt += '3) Print to screen\n'
        opt += '4) Save\n'
        opt += '5) Exit\n'
        while 1:
            print(opt)
            choice = input('Enter option number: ')
            if choice not in [1,2,3,4,5]:
                print('Invalid option: '+choice+', try again')
            else:
                return int(choice)       
#==========================================================
def main():
    ''' 
    function testing.
    '''
    SocialPerson.get_sp()
    command = 0
    while command != '5' or command != '5)':
        command = SocialPerson.get_option()
        if command == '1' or command == '1)':
            SocialPerson.add_friend()
        if command== '2' or command == '2)':
            SocialPerson.unfriend()
        if command == '3' or command == '3)':
            SocialPerson.unfriend()
        if command == '4' or command == '4)':
            SocialPerson.unfriend()
    fname = input('Enter save filename: ')
if __name__ == '__main__':
    main()
