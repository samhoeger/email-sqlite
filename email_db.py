'''
Description:
Creates a sqlite database filled with people objects, separated into a friends table and colleagues table.
This database is used for an emailer list.
'''
import sqlite3
import os
import sys

class Person:
    def __init__(self, first = "", last = "", bday = "", email = ""):
        if first !='':
            self.first = first
        else:
            self.first = input("Enter person's first name: ")
        if last !='':
            self.last = last
        else:
            self.last = input("Enter person's last name: ")
        if bday !='':
            self.bday = bday
        else:
            self.bday = input("Enter person's birthday: ")
        if email != '':
            self.email = email
        else:
            self.email = input("Enter person's e-mail: ")

    def __repr__(self):
        return self.first +  ' ' + self.last + ': ' + self.bday + ', ' + self.email
    
    def write_person(self,file_obj):
        
        file_obj.write(self.first+'\n'+self.last + '\n'+ self.bday + '\n' + self.email+'\n')
    
    @classmethod
    def read_person(cls, file):
        first = file.readline().strip()
        if not first:
            return False
        last = file.readline().strip()
        bday = file.readline().strip()
        email = file.readline().strip()
        return cls(first, last, bday, email)
def open_persons_db():
    p_exists = os.path.exists('persons.db')
    db = sqlite3.connect('persons.db')
    db.row_factory = sqlite3.Row
    if p_exists != True:
        db.execute('CREATE TABLE friends'+'(first TEXT,last TEXT,bday TEXT, email TEXT PRIMARY KEY)')
        db.commit()
        db.execute('CREATE TABLE colleagues'+'(first TEXT,last TEXT,bday TEXT, email TEXT PRIMARY KEY)')
        db.commit()
        return db
    return db
def add_person(db,person,friend=True,colleague=False):
    if friend==False and colleague==False:
        print('Warning: '+person.email+' not added - must be friend or colleague',file=sys.stderr)
        return False
    if friend==True:
        db.execute('INSERT INTO friends (first,last,bday,email) VALUES (?,?,?,?);',(person.first,person.last,person.bday,person.email))
    if colleague==True:
        db.execute('INSERT INTO colleagues (first,last,bday,email) VALUES (?,?,?,?);',(person.first,person.last,person.bday,person.email))
    return True
def delete_person(db,person):
    db.execute('DELETE FROM friends WHERE email = ?;',(person.email,))
    db.commit()
    db.execute('DELETE FROM colleagues WHERE email = ?;',(person.email,))
    db.commit()
def to_Person_list(cursor):
    lst = []
    rows = cursor.fetchall()
    for row in rows:
        row = tuple(row)
        lst.append(Person(row[0],row[1],row[2],row[3]))
    return lst
def get_friends(db):
    cursor = db.execute('SELECT * FROM friends;')
    return to_Person_list(cursor)
def get_colleagues(db):
    cursor = db.execute('SELECT * FROM colleagues;')
    return to_Person_list(cursor)
def get_all(db):
    cursor = db.execute('SELECT * FROM friends JOIN colleagues ON friends.email = colleagues.email;')
    
            
#==========================================================
def main():
    '''
    function testing.
    '''
    

if __name__ == '__main__':
    main()
